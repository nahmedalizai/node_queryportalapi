const uuidV4 = require('uuid/v4');
var config = require('./config');
var bcrypt = require('bcryptjs');
const Pool = require('pg').Pool;
const fs = require('fs');
const blobToBase64 = require('blob-to-base64')

const pool = new Pool({
  user: config.db.username,
  host: config.db.host,
  database: config.db.dbName,
  password: config.db.password,
  port: config.db.port,
});

function getFormatedDate() {
    var date = new Date();
    var yyyy = date.getFullYear();
    var MM = date.getMonth();
    MM = MM+1;
    if (MM < 10)
      MM = '0'+ MM;
    var DD = date.getDate();
    if(DD < 10)
      DD = '0'+ DD;
  
    return (yyyy + '' + MM + '' + DD);
  }

function logError(error) {
    var date = new Date();
    var fileName = 'errorlog_'+getFormatedDate()+date.getSeconds()+date.getMilliseconds()+'.txt';
    var errorMessage = 'Error Code & timepstamp: ' + error.code + ' - ' + date +  ', Message : ' + error.message + ', Error Stack : ' + error.stack;
    fs.writeFile(fileName, errorMessage, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("Error saved into the file : " + fileName);
    }); 
}

function logResults(results) {
  var date = new Date();
  var fileName = 'errorlog_'+getFormatedDate()+date.getSeconds()+date.getMilliseconds()+'.txt';
  var errorMessage = results;
  fs.writeFile(fileName, errorMessage, function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("Results saved into the file : " + fileName);
  }); 
}

  const addUser = (request, response) => {
    if(config.adminUser.username !== "" && request.query.vrp_username.toUpperCase() === config.adminUser.username.toUpperCase()) {
      response.status(202).send('For admin user, refer to deployment guide')
    }
    else {
      pool.query('SELECT COUNT(id) FROM users WHERE vrp_username=$1' ,[request.query.vrp_username.toUpperCase()], (error, results) => {
        if (error) {
          logError(error)
              response.status(500).send('An error occured in the API, please check server logs')
        }
        else {
          if(results.rows[0].count === '0') {
              pool.query('INSERT INTO users(vrp_name, vrp_email, vrp_username, vrp_password, vrp_teamid, vrp_roleid, vrp_isactive) VALUES ($1, $2, $3, $4, $5, $6, $7)' ,
              [request.query.vrp_name, request.query.vrp_email, request.query.vrp_username.toUpperCase(), bcrypt.hashSync(request.query.vrp_password, bcrypt.genSaltSync(10)),request.query.vrp_teamid, request.query.vrp_roleid, true], (error, results) => {
                if (error) {
                  logError(error)
                  response.status(500).send('An error occured in the API, please check server logs')
                }
                else{
                  if(results.rowCount > 0)
                    response.status(201).send('User added')
                  else {
                    logResults(results)
                    response.status(202).send('Adding user failed, Please contact admin')
                  }
                }
                
              })      
            }
          else
            response.status(202).send('Username already exists')
        }
      })
    }
  }

  const verifyUser = (request, response) => {
    var userDetails = {}
    if(request.query.username.toUpperCase() === config.adminUser.username.toUpperCase() && request.query.password === config.adminUser.password){
      userDetails = {
          displayName: "Admin",
          emailAddress: "",
          username: config.adminUser.username,
          teamCode: "ADM",
          roleCode: "CRUD" 
      }
      response.status(200).json({isSuccess: true, userDetails})
    }
    else {
      pool.query('SELECT u.vrp_name, u.vrp_email, u.vrp_username, u.vrp_password, t.vrp_teamcode, r.vrp_rolecode, u.vrp_isactive FROM users u, teams t, roles r ' +
                'WHERE u.vrp_username = $1 AND u.vrp_isactive = $2 AND u.vrp_teamid=t.id AND u.vrp_roleid=r.id',[request.query.username.toUpperCase(), true], (error, results) => {
        if (error) {
            logError(error)
            response.status(500).send('An error occured in the API, please check server logs')
          }
        else {
            if(results.rows.length > 0) {
                results.rows[0]
                if(bcrypt.compareSync(request.query.password, results.rows[0].vrp_password))
                {
                    userDetails = {
                        displayName: results.rows[0].vrp_name,
                        emailAddress: results.rows[0].vrp_email,
                        username: results.rows[0].vrp_username,
                        teamCode: results.rows[0].vrp_teamcode,
                        roleCode: results.rows[0].vrp_rolecode 
                    }
                    response.status(200).json({isSuccess: true, userDetails})
                }
                else
                  response.status(202).send({isSuccess: false}) 
              }
              else
                response.status(202).send({isSuccess: false})
        }
      })
    }
  }

  const changeUserStatus = (request, response) => {
        pool.query('UPDATE users SET vrp_isactive =$1 WHERE vrp_username = $2',[request.query.status,request.query.username], (error, results) => {
            if (error) {
                logError(error)
                response.status(500).send('An error occured in the API, please contact admin')
              }
            else {
                if(results.rowCount > 0) {
                    response.status(201).send('Username : ' + request.query.username + ' status changed')
                  }
                  else
                    response.status(202).send('Invalid Username')
            }
          })
  }

  const changeUserPassword = (request, response) => {
    pool.query('UPDATE users SET vrp_password =$1 WHERE vrp_username = $2',[(bcrypt.hashSync(request.query.password, bcrypt.genSaltSync(10))),request.query.username], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
          if(results.rowCount > 0) {
              response.status(201).send('Username : ' + request.query.username + ' password changed')
            }
            else
              response.status(202).send('Invalid Username')
      }
    })
  }

  const updateUserDetails = (request, response) => {
    pool.query('UPDATE users SET ' + 
                'vrp_name = $1, ' +
                'vrp_email = $2, ' +
                'vrp_teamid = $3, ' +
                'vrp_roleid = $4, ' +
                'vrp_isactive = $5 ' +
                'WHERE vrp_username = $6 AND id = $7',[request.query.name,request.query.email,request.query.teamid,request.query.roleid,request.query.active,request.query.username,request.query.id], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
          if(results.rowCount > 0) {
              response.status(201).send('User information updated')
            }
            else
              response.status(202).send('Updating user failed')
      }
    })
  }

  const getUserDetails = (request, response) => {
    pool.query('SELECT id, vrp_name, vrp_email, vrp_username, vrp_teamid, vrp_roleid, vrp_isactive ' +
                'FROM users WHERE vrp_name = $1 OR vrp_username= $2',[request.query.searchkey,request.query.searchkey.toUpperCase()], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
          if(results.rowCount > 0) {
              response.status(200).json(results.rows[0])
            }
            else
              response.status(202).send('User not found')
      }
    })
  }

  const getCategories = (request, response) => {
    pool.query('SELECT id, vrp_name FROM categories', (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
        response.status(200).json(results.rows)
      }
    })
}

const getSubcategoriesById = (request, response) => {
    pool.query('SELECT id, vrp_name, vrp_categoryid FROM subcategories WHERE vrp_categoryid=$1',[request.query.id], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
        response.status(200).json(results.rows)
      }
    })
}

const addCategory = (request, response) => {
    pool.query('SELECT id FROM categories WHERE vrp_name = $1', [request.query.categoryname], (error, results) => {
        if (error) {
            logError(error)
            response.status(500).send('An error occured in the API, please contact admin')
          }
        else {
            if(results.rowCount=== 0) {
                pool.query('INSERT INTO categories(vrp_name) VALUES ($1) RETURNING id',[request.query.categoryname], (error, results) => {
                    if (error) {
                        logError(error)
                        response.status(500).send('An error occured in the API, please contact admin')
                      }
                    else {
                      if(results.rowCount > 0) {
                          response.status(201).send(results.rows)
                        }
                        else {
                          logResults(results)
                          response.status(200).send('Adding category failed')
                        }
                      }
                  })
            }
            else 
               response.status(202).send(results.rows)
        }
      })
}

const addSubCategory = (request, response) => {
    pool.query('SELECT COUNT(id) FROM subcategories WHERE vrp_categoryid=$1 AND vrp_name= $2', [request.query.categoryid, request.query.subcategoryname], (error, results) => {
        if (error) {
            logError(error)
            response.status(500).send('An error occured in the API, please contact admin')
          }
        else {
            if(results.rows[0].count === '0') {
                pool.query('INSERT INTO subcategories(vrp_categoryid,vrp_name) VALUES ($1,$2) ',[request.query.categoryid, request.query.subcategoryname], (error, results) => {
                    if (error) {
                        logError(error)
                        response.status(500).send('An error occured in the API, please contact admin')
                      }
                    else {
                      if(results.rowCount > 0) {
                          response.status(201).send('Sub Category added')
                        }
                        else{
                          logResults(results)
                          response.status(200).send('Adding subcategory failed')
                        }
                        
                    }
                  })
            }
            else 
                response.status(202).send('Sub Category already exists')
        }
      })
}

const addNewQuestion = (request, response) => {
          pool.query('INSERT INTO questions ' + 
          '(vrp_title, vrp_description, vrp_questiontype, vrp_categoryid, vrp_subcategoryid, vrp_status, vrp_addedon, vrp_addedby, vrp_hasverifiedanswer, vrp_attachmentid,vrp_modifiedon) ' +
          'VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)',[
                                                                  request.query.title,
                                                                  request.query.description,
                                                                  request.query.questiontypeid,
                                                                  request.query.categoryid,
                                                                  request.query.subcategoryid,
                                                                  "Open",
                                                                  getFormatedDate(),
                                                                  request.query.user,
                                                                  false,
                                                                  request.query.attachmentid,
                                                                  (new Date())
                                                              ], (error, results) => {
            if (error) {
              logError(error)
              response.status(500).send('An error occured in the API, please contact admin')
            }
            else {
              if(results.rowCount > 0)
                response.status(201).send('New question added')
              else {
                logResults(results)
                response.status(202).send('Adding new question failed, Please contact admin')
              }    
            }
          })
}

const addAttachment = (request, response) => {
  fs.readFile(request.files.file.path, 'hex', function(err, imgData) {
    imgData = '\\x' + imgData;
    pool.query('INSERT INTO attachments (vrp_attachment) VALUES ($1) RETURNING id',[imgData], 
      function(err, writeResult) {
        if(err)
        {
          logError(err)
          response.status(500).send('An error occured in the API, please contact admin')
        }
        else {
          if(writeResult.rowCount > 0) {
            //console.log('err',err,'pg writeResult',writeResult);
            response.status(201).send({isSuccess: true, attachmentid: writeResult.rows[0].id})
          }
          else {
            logResults(results)
            response.status(202).send('Adding attachment failed, Please contact admin')
          }
        }
    });
  });
}

const getAttachment = (request, response) => {
  pool.query('SELECT vrp_attachment FROM attachments WHERE id =$1', [request.query.id], 
    function(err, readResult) {
      if(readResult.rowCount > 0)
        response.status(200).send(readResult.rows)
      else {
        logError(err)
        response.status(202).send('Error in getting attachment')
      }
      // var data = readResult.rows[0].vrp_attachment;
      // var myBuffer = new Buffer(data.length);
      // for (var i = 0; i < data.length; i++) {
      //     myBuffer[i] = data[i];
      // }
      // fs.writeFile("image.jpg", myBuffer, function(err) {
      //     if(err) {
      //         console.log(err);
      //     } else {
      //         console.log("The file was saved!");
      //     }
      // });
      
    });
}

const getQuestions = (request, response) => {
  if(request.query.filterType !== undefined && request.query.filterType !== null && request.query.filterType !== "1") {
    if(request.query.categoryid === "0")
      filteredDataAll(request,response)
    else 
      filteredDataByCategory(request,response)
  }
  else { // ALL
    pool.query('SELECT (A.vrp_description) AS latestreply, (A.id) AS answerid, (A.vrp_isverified) AS isverifiedanswer, Q.* ' +
              'FROM questions Q ' +
              'LEFT OUTER JOIN ' +
              '(SELECT DISTINCT ON (vrp_questionid) id, vrp_description, vrp_questionid, vrp_isverified FROM answers ORDER BY vrp_questionid, id DESC) A ' +
              'ON Q.id = A.vrp_questionid ' +
              'ORDER BY vrp_modifiedon DESC ', (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
        response.status(200).json(results.rows)
      }
    })
  }
}

function filteredDataAll (request,response) {
  var condition = ''
  var value = ''
  if(request.query.filterType === "6") { // Not Verified yet
    condition = 'WHERE A.id IS NOT NULL AND vrp_status = $1 and vrp_hasverifiedanswer = $2'
    pool.query('SELECT (A.vrp_description) AS latestreply, (A.id) AS answerid, (A.vrp_isverified) AS isverifiedanswer, Q.* ' +
              'FROM questions Q ' +
              'LEFT OUTER JOIN ' +
              '(SELECT DISTINCT ON (vrp_questionid) id, vrp_description, vrp_questionid, vrp_isverified FROM answers ORDER BY vrp_questionid, id DESC) A ' +
              'ON Q.id = A.vrp_questionid ' +
                condition +
              ' ORDER BY vrp_modifiedon DESC ',['Open', false], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
        response.status(200).json(results.rows)
      }
    })
  }
  else {
    if(request.query.filterType === "2"){ // Status Open
      value = 'Open'
      condition = 'WHERE vrp_status = $1'
    }
    else if(request.query.filterType === "3"){ // Status Closed
      value = 'Closed'
      condition = 'WHERE vrp_status = $1' 
    }
    else if(request.query.filterType === "4"){ // Posted by User
      if(request.query.filterValue !== undefined && request.query.filterValue !== null)
        value = request.query.filterValue
      else
        value = 'no user'
      condition = 'WHERE vrp_addedby = $1' 
    }
    else if(request.query.filterType === "5"){ // Not Answered
      value = 'Open'
      condition = 'WHERE A.id IS NULL AND vrp_status = $1' 
    }
    pool.query('SELECT (A.vrp_description) AS latestreply, (A.id) AS answerid, (A.vrp_isverified) AS isverifiedanswer, Q.* ' +
              'FROM questions Q ' +
              'LEFT OUTER JOIN ' +
              '(SELECT DISTINCT ON (vrp_questionid) id, vrp_description, vrp_questionid, vrp_isverified FROM answers ORDER BY vrp_questionid, id DESC) A ' +
              'ON Q.id = A.vrp_questionid ' +
                condition +
              ' ORDER BY vrp_modifiedon DESC ',[value], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
        response.status(200).json(results.rows)
      }
    })
  }
}

function filteredDataByCategory (request,response) {
  var condition = ''
  var value = ''
  if(request.query.filterType === "6") { // Not Verified yet
    condition = 'WHERE A.id IS NOT NULL AND vrp_status = $1 AND vrp_hasverifiedanswer = $2 AND vrp_categoryid = $3'
    pool.query('SELECT (A.vrp_description) AS latestreply, (A.id) AS answerid, (A.vrp_isverified) AS isverifiedanswer, Q.* ' +
              'FROM questions Q ' +
              'LEFT OUTER JOIN ' +
              '(SELECT DISTINCT ON (vrp_questionid) id, vrp_description, vrp_questionid, vrp_isverified FROM answers ORDER BY vrp_questionid, id DESC) A ' +
              'ON Q.id = A.vrp_questionid ' +
                condition +
              ' ORDER BY vrp_modifiedon DESC ',['Open', false,request.query.categoryid], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
        response.status(200).json(results.rows)
      }
    })
  }
  else {
    if(request.query.filterType === "2"){ // Status Open
      value = 'Open'
      condition = 'WHERE vrp_status = $1 AND vrp_categoryid = $2'
    }
    else if(request.query.filterType === "3"){ // Status Closed
      value = 'Closed'
      condition = 'WHERE vrp_status = $1 AND vrp_categoryid = $2'
    }
    else if(request.query.filterType === "4"){ // Posted by User
      if(request.query.filterValue !== undefined && request.query.filterValue !== null)
        value = request.query.filterValue
      else
        value = 'no user'
      condition = 'WHERE vrp_addedby = $1 AND vrp_categoryid = $2' 
    }
    else if(request.query.filterType === "5"){ // Not Answered
      value = 'Open'
      condition = 'WHERE A.id IS NULL AND vrp_status = $1 AND vrp_categoryid = $2'
    }
    pool.query('SELECT (A.vrp_description) AS latestreply, (A.id) AS answerid, (A.vrp_isverified) AS isverifiedanswer, Q.* ' +
              'FROM questions Q ' +
              'LEFT OUTER JOIN ' +
              '(SELECT DISTINCT ON (vrp_questionid) id, vrp_description, vrp_questionid, vrp_isverified FROM answers ORDER BY vrp_questionid, id DESC) A ' +
              'ON Q.id = A.vrp_questionid ' +
                condition +
              ' ORDER BY vrp_modifiedon DESC ',[value, request.query.categoryid], (error, results) => {
      if (error) {
          logError(error)
          response.status(500).send('An error occured in the API, please contact admin')
        }
      else {
        response.status(200).json(results.rows)
      }
    })
  }
}

const addNewAnswer = (request, response) => {
  pool.query('INSERT INTO answers(vrp_description, vrp_answeredby, vrp_answeredon, vrp_isaccepted, vrp_isverified, vrp_questionid) VALUES ($1, $2, $3, $4, $5, $6)',[
                                                          request.query.description,
                                                          request.query.user,
                                                          getFormatedDate(),
                                                          false,
                                                          false,
                                                          request.query.questionid
                                                      ], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please contact admin')
    }
    else {
      if(results.rowCount > 0){
        pool.query('UPDATE questions SET vrp_modifiedon = $1 WHERE id = $2',[new Date(),request.query.questionid])
        response.status(201).send('Reply added')
      }
      else {
        logResults(results)
        response.status(202).send('Reply failed, Please contact admin')
      }    
    }
  })
}

const getAnswersById = (request, response) => {
  pool.query('SELECT id, vrp_description, vrp_answeredby, vrp_answeredon, vrp_isaccepted, vrp_isverified, vrp_questionid ' + 
              'FROM answers WHERE vrp_questionid = $1 ORDER BY id DESC',[request.query.questionid], (error, results) => {
    if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      response.status(200).json(results.rows)
    }
  })
}

const verifyAnswerById = (request, response) => {
  pool.query('UPDATE answers SET vrp_isverified = $1 WHERE id = $2',[true,request.query.answerid], (error, results) => {
    if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      if(results.rowCount > 0){
        pool.query('UPDATE questions SET vrp_hasverifiedanswer = $1 WHERE id = $2',[true, request.query.questionid])
        response.status(201).send('Answer marked verified')
      }
      else {
        logResults(results)
        response.status(202).send('Marking answer verify failed, Please contact admin')
      }    
    }
  })
}

const insertTimestamp = (request, response) => {
  pool.query('Insert into logs (modifiedon) Values($1)',[new Date()], (error, results) => {
    if (error) {
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      if(results.rowCount > 0){
        response.status(201).send('added')
      }
      else {
        logResults(results)
        response.status(202).send('not added')
      }    
    }
  })
}

const changeQuestionStatusById = (request, response) => {
    pool.query('UPDATE questions SET vrp_status = $1, vrp_modifiedon = $2 WHERE id = $3',[request.query.status,new Date(),request.query.id], (error, results) => {
      if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
      else {
        if(results.rowCount > 0) {
            response.status(201).send('Question closed')
        }
        else {
            logResults(results)
            response.status(202).send('Closing question failed, Please contact admin')
        }
      }
    });
}

const getCategoryNameById = (request, response) => {
  pool.query('SELECT vrp_name FROM categories WHERE id=$1',[request.query.id], (error, results) => {
    if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      response.status(200).json(results.rows)
    }
  })
}

const getSubcategoryNameById = (request, response) => {
  pool.query('SELECT (S.vrp_name) AS subcategory, (C.vrp_name) AS category FROM subcategories S, categories C WHERE S.vrp_categoryid = C.id AND S.id = $1',[request.query.id], (error, results) => {
    if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      response.status(200).json(results.rows)
    }
  })
}

const updateAnswerById = (request, response) => {
  pool.query('UPDATE answers SET vrp_description = $1 WHERE id = $2',[request.query.description,request.query.id], (error, results) => {
    if (error) {
      logError(error)
      response.status(500).send('An error occured in the API, please contact admin')
    }
    else {
      if(results.rowCount > 0) {
          response.status(201).send('Answer updated')
      }
      else {
          logResults(results)
          response.status(202).send('Updating answer failed, Please contact admin')
      }
    }
  });
}

const getRoles = (request, response) => {
  pool.query('SELECT * FROM roles', (error, results) => {
    if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      response.status(200).json(results.rows)
    }
  })
}

const getTeams = (request, response) => {
  pool.query('SELECT * FROM teams', (error, results) => {
    if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      response.status(200).json(results.rows)
    }
  })
}

const getQuestionById = (request, response) => {
  pool.query('SELECT * FROM questions WHERE id = $1',[request.query.id], (error, results) => {
    if (error) {
        logError(error)
        response.status(500).send('An error occured in the API, please contact admin')
      }
    else {
      response.status(200).json(results.rows)
    }
  })
}

module.exports = {
    addUser,
    verifyUser,
    changeUserStatus,
    changeUserPassword,
    updateUserDetails,
    getUserDetails,
    getCategories,
    getSubcategoriesById,
    addCategory,
    addSubCategory,
    addNewQuestion,
    addAttachment,
    getAttachment,
    getQuestions,
    addNewAnswer,
    getAnswersById,
    verifyAnswerById,
    insertTimestamp,
    changeQuestionStatusById,
    getCategoryNameById,
    getSubcategoryNameById,
    updateAnswerById,
    getRoles,
    getTeams,
    getQuestionById
}