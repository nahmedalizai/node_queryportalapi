const express = require('express')
const formData = require('express-form-data')
const bodyParser = require('body-parser')
var config = require('./config')
const app = express()
const port = config.appSettings.port
const db = require('./queries')

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(formData.parse())

app.use(bodyParser.json())

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.post('/adduser', db.addUser)
app.get('/login', db.verifyUser)
app.post('/changeuserstatus', db.changeUserStatus)
app.post('/changeuserpassword', db.changeUserPassword)
app.post('/updateuserdetails', db.updateUserDetails)
app.get('/getuserdetails',db.getUserDetails)
app.get('/getcategories', db.getCategories)
app.get('/getsubcategoriesbyid', db.getSubcategoriesById)
app.post('/addcategory', db.addCategory)
app.post('/addsubcategory', db.addSubCategory)
app.post('/addnewquestion', db.addNewQuestion)
app.post('/addattachment',db.addAttachment)
app.get('/getattachment', db.getAttachment)
app.get('/getquestions', db.getQuestions)
app.post('/addnewanswer', db.addNewAnswer)
app.get('/getanswersbyid', db.getAnswersById)
app.post('/verifyanswerbyid', db.verifyAnswerById)
app.post('/inserttimestamp', db.insertTimestamp)
app.post('/changequestionstatusbyid', db.changeQuestionStatusById)
app.get('/getcategorynamebyid',db.getCategoryNameById)
app.get('/getsubcategorynamebyid',db.getSubcategoryNameById)
app.post('/updateanswerbyid', db.updateAnswerById)
app.get('/getroles',db.getRoles)
app.get('/getteams',db.getTeams)
app.get('/getquestionbyid',db.getQuestionById)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API', Application: config.appName, Version: config.version })
  })

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
  })

app.use((req, res) => {
  console.log(req.body);
  res.on("finish", () => {
    console.log(res);
  });
});